// I assume that user sends a request to the backend and the task is to check if he/she is using proxy or not
const checkProxy = (object) => Object.keys(object).includes("X-Forwarded-For");

const reqHeaders = {
    "X-Forwarded-For": "192.168.1.10"
};

console.log(checkProxy(reqHeaders) ? 404 : 200);