const ImperativeCallOnEveryChild = (array, fun) => {
    const newArray = [];
    for(let i = 0 ; i < array.length ; i ++) {
        newArray.push(fun(array[i]))
    }
    return newArray;
};

const DeclaritiveCallOnEveryChild = (array, fun) => array.map(element => fun(element));

const createNewArray = (number) => number * 2;

const array = [1, 2, 3, 4, 5];

console.log(`Imperative function result: ${ImperativeCallOnEveryChild(array, createNewArray)}`);
console.log(`Declaritive function result: ${DeclaritiveCallOnEveryChild(array, createNewArray)}`);